<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Exercise;
use App\Entity\Group;
use App\Entity\Pack;
use App\Entity\Question;
use App\Entity\Subject;
use App\Entity\Transaction;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');

        // CUSTOM SUPER-ADMIN, ADMIN, PROFESSOR, STUDENT
        $user1 = new User();
        $user1->setFirstName($faker->firstName);
        $user1->setLastName($faker->lastName);
        $user1->setEmail('sadmin@sadmin.fr');
        $user1->setRoles(['ROLE_SUPER_ADMIN']);
        $user1->setPassword($this->encoder->encodePassword($user1, "mdpmdpmdp"));

/*        $user2 = new User();
        $user2->setFirstName($faker->firstName);
        $user2->setLastName($faker->lastName);
        $user2->setEmail('admin@admin.fr');
        $user2->setRoles(['ROLE_ADMIN']);
        $user2->setPassword($this->encoder->encodePassword($user2, "mdpmdpmdp"));

        $user3 = new User();
        $user3->setFirstName($faker->firstName);
        $user3->setLastName($faker->lastName);
        $user3->setEmail('prof@prof.fr');
        $user3->setRoles(['ROLE_PROFESSOR']);
        $user3->setPassword($this->encoder->encodePassword($user3, "mdpmdpmdp"));

        $user4 = new User();
        $user4->setFirstName($faker->firstName);
        $user4->setLastName($faker->lastName);
        $user4->setEmail('student@student.fr');
        $user4->setRoles(['ROLE_STUDENT']);
        $user4->setPassword($this->encoder->encodePassword($user4, "mdpmdpmdp"));

        $user5 = new User();
        $user5->setFirstName($faker->firstName);
        $user5->setLastName($faker->lastName);
        $user5->setEmail('admin2@admin2.fr');
        $user5->setRoles(['ROLE_ADMIN']);
        $user5->setPassword($this->encoder->encodePassword($user5, "mdpmdpmdp"));*/

        // CUSTOM PACKS
        $pack1 = new Pack();
        $pack1->setName("basic");
        $pack1->setPrice(39.99);
        $pack1->setOldPrice(39.99);
        $pack1->setNbStudents(50);
        $pack1->setNbrProfessors(5);
        $pack1->setPromotion(false);
        $pack1->setStripePrice("price_1JWodjH1ST2SneRlnfUo9Qea");

        $pack2 = new Pack();
        $pack2->setName("classic");
        $pack2->setPrice(89.99);
        $pack2->setOldPrice(89.99);
        $pack2->setNbStudents(200);
        $pack2->setNbrProfessors(20);
        $pack2->setPromotion(false);
        $pack2->setStripePrice("price_1JkD4QH1ST2SneRl8wS6PPfA");

        $pack3 = new Pack();
        $pack3->setName("prenium");
        $pack3->setPrice(107.99);
        $pack3->setNbStudents(800);
        $pack3->setNbrProfessors(80);
        $pack3->setPromotion(true);
        $pack3->setFreeDuration(3);
        $pack3->setTypeDuration("month(s)");
        $pack3->setOldPrice(119.99);
        $pack3->setStripePrice("price_1JkD5NH1ST2SneRlIZrupx2f");


        $pack4 = new Pack();
        $pack4->setName("elite");
        $pack4->setPrice(199.99);
        $pack4->setOldPrice(199.99);
        $pack4->setNbStudents(2000);
        $pack4->setNbrProfessors(200);
        $pack4->setPromotion(false);
        $pack4->setStripePrice("price_1JkD6wH1ST2SneRlHZtpuYqE");

      /*  $user5->setPack($pack4);
        $user2->setPack($pack1);*/

        /*$groups = array();

        // CREATE 15 GROUPS
        for ($class = 0; $class < 15; $class++) {
            $groups[$class] = new Group();
            $groups[$class]->setName('class ' . $class);
        }

        $admin_users = array();

        // CREATE 500 USERS (ADMINS + PROFESSORS + STUDENTS)
        for ($nbUsers = 0; $nbUsers < 500; $nbUsers++) {
            $user = new User();
            $user->setFirstName($faker->firstName);
            $user->setLastName($faker->lastName);
            $user->setEmail($faker->unique()->email);
            $user->setPassword($this->encoder->encodePassword($user, "mdpmdpmdp"));

            // CREATE 3 ADMINS
            if ($nbUsers < 3) {
                $user->setRoles(["ROLE_ADMIN"]);
                $random = $faker->numberBetween(5, 20);
                if ($nbUsers == 0) {
                    for($i = 0; $i < 5; $i++) {
                        $user->addGroup($groups[$i]);
                    }
                } elseif($nbUsers == 1) {
                    for($i = 5; $i < 10; $i++) {
                        $user->addGroup($groups[$i]);
                    }
                } elseif($nbUsers == 2) {
                    for($i = 10; $i < 15; $i++) {
                        $user->addGroup($groups[$i]);
                    }
                }
                
                for ($nbTransaction = 0; $nbTransaction < $random; $nbTransaction++) {
                    $transaction = new Transaction();
                    $transaction->setStartDate($faker->datetime);
                    $transaction->setEndDate($faker->datetime);
                    $transaction->setDescription($faker->text(100));
                    $transaction->setSubTotal(200);
                    $transaction->setTotal(1400);
                    $user->addTransaction($transaction);
                    $randomPack = $faker->numberBetween(1, 4);
                    ${'pack' . $randomPack}->addTranstion($transaction);
                    $user->setPack(${'pack' . $randomPack});

                    $manager->persist($transaction);
                }
                $admin_users[$nbUsers] = $user;
                // CREATE 27 PROFESSORS
            } elseif ($nbUsers < 30) {
                $user->setRoles(["ROLE_PROFESSOR"]);
                if ($nbUsers % 2 == 0) {
                    $user->setAdminProfessor($user2);
                } else {
                    $user->setAdminProfessor($user5);
                }
                $nb = $nbUsers % 10;
                $subject = new Subject();
                $subject->setName($faker->word);
                for ($nbExo = 0; $nbExo < 5; $nbExo++) {
                    $exo = new Exercise();
                    $exo->setName('exercise ' . $nbExo);
                    $exo->setDescription($faker->text(100));
                    for ($i = $nb; $i < ($nb + 5); $i++) {
                        $groups[$i]->addGroupProfessor($user);
                        $manager->persist($groups[$i]);
                        $exo->addGroup($groups[$i]);
                    }
                    for ($nbQuestion = 0; $nbQuestion < 3; $nbQuestion++) {
                        $question = new Question();
                        $question->setName('question ' . $nbQuestion);
                        $question->setDescription($faker->text(100));
                        $question->setDifficulty('easy');
                        $type = $nbQuestion === 1 ? 'qcm' : 'libre';
                        $question->setType($type);
                        if ($type === 'libre') {
                            $question->setCorrectAnswer('test');
                        } else {
                            for ($j = 0; $j < 5; $j++) {
                                $question->addOtherAnswer($faker->word);
                            }
                            $question->setCorrectAnswer('test');
                        }
                        $exo->addQuestion($question);
                        $manager->persist($question);
                    }
                    $comment = new Comment();
                    $comment->setText($faker->text(400));
                    $comment->setCreatedAt($faker->datetime);
                    $exo->addComment($comment);
                    $user->addComment($comment);
                    $subject->addExercise($exo);

                    $manager->persist($comment);
                    $manager->persist($exo);
                }
                $wichOne = $nbUsers % 3;
                $admin_users[$wichOne]->addSubject($subject);
                $manager->persist($subject);

                // CREATE 467 STUDENTS
            } else {
                $user->setRoles(["ROLE_STUDENT"]);
                if ($nbUsers % 2 == 0) {
                    $user->setAdminStudent($user2);
                } else {
                    $user->setAdminStudent($user5);
                }

                $nb = $nbUsers % 15;
                $groups[$nb]->addGroupStudent($user);
                $manager->persist($groups[$nb]);
            }

            if ($nbUsers >= 3) {
                $manager->persist($user);
            }
        }

        $user4->setAdminStudent($admin_users[0]);
        $user3->setAdminProfessor($admin_users[0]);

        $manager->persist($admin_users[0]);
        $manager->persist($admin_users[1]);
        $manager->persist($admin_users[2]);

        $manager->persist($user1);
        $manager->persist($user2);
        $manager->persist($user3);
        $manager->persist($user4);
        $manager->persist($user5);
        */
        $manager->persist($pack1);
        $manager->persist($pack2);
        $manager->persist($pack3);
        $manager->persist($pack4);

        $manager->flush();
    }
}
