<?php
// src/Controller/MailerController.php
namespace App\Services;

use App\Entity\Pack;
use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;
use Twig\Environment;

class MailerService
{

    private $mailerInterface;
    private $twig;
    private $resetPasswordHelper;

    public function __construct(MailerInterface $mailerInterface, Environment $twig,ResetPasswordHelperInterface $resetPasswordHelper)
    {
        $this->mailerInterface = $mailerInterface;
        $this->twig = $twig;
        $this->resetPasswordHelper = $resetPasswordHelper;
    }
    public function sendEmail(User $user): void
    {
        $email = (new TemplatedEmail())
            ->from(new Address('mbouhadjar1@myges.fr', 'Moodle'))
            ->to($user->getEmail())
            ->subject('Your password reset request')
            ->htmlTemplate('reset_password/email-password.html.twig')
            ->context([
                'user'=>$user,
                'resetToken' => $this->resetPasswordHelper->generateResetToken($user),
                'tokenLifetime' => $this->resetPasswordHelper->getTokenLifetime(),
            ]);

        $this->mailerInterface->send($email);
    }


    public function sendEmailPaymentPack(User $user,Pack $pack): void
    {
        $email = (new TemplatedEmail())
            ->from(new Address('mbouhadjar1@myges.fr', 'Moodle'))
            ->to($user->getEmail())
            ->subject('Subscribe for pack')
            ->htmlTemplate('admin/payment/payment-pack.html.twig')
            ->context([
                'pack'=>$pack
            ]);

        $this->mailerInterface->send($email);
    }

    public function sendEmailPaymentChangePlanPack(User $user,Pack $pack): void
    {
        $email = (new TemplatedEmail())
            ->from(new Address('mbouhadjar1@myges.fr', 'Moodle'))
            ->to($user->getEmail())
            ->subject('Change service plan')
            ->htmlTemplate('admin/payment/payment-pack-change.html.twig')
            ->context([
                'pack'=>$pack
            ]);

        $this->mailerInterface->send($email);
    }
}
