<?php

namespace App\Twig;

use App\Services\DateDiffService;
use DateTime;
use Doctrine\Common\Collections\Collection;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class DateDiffExtension extends AbstractExtension
{

    protected $service;

    public function __construct(DateDiffService $service)
    {
        $this->service = $service;
    }

    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('dateDiff', [$this, 'dateDiffFromNow']),
        ];
    }

    public function dateDiffFromNow(DateTime $date): string
    {
        return $this->service->dateDiffInString($date);
    }
}
