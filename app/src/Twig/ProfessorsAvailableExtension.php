<?php

namespace App\Twig;

use App\Repository\GroupRepository;
use Doctrine\Common\Collections\Collection;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class ProfessorsAvailableExtension extends AbstractExtension
{
    protected $groupRepository;

    public function __construct(GroupRepository $groupRepository)
    {
        $this->groupRepository = $groupRepository;
    }

    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('professorsAvailable', [$this, 'professorAvailableByGroup']),
        ];
    }

    public function professorAvailableByGroup($void, $groupId, $user)
    {
        $professorsAvailable = [];
        $group = $this->groupRepository->find($groupId);

        $professors = $user->getProfessors();

        foreach($professors as $professor) {
            if (!$group->getGroupProfessors()->contains($professor) && !is_null($professor->getSubject())) {
                $professorsAvailable[] = $professor;
            }
        }
        return $professorsAvailable;
    }
}