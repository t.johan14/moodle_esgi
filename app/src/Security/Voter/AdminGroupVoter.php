<?php

namespace App\Security\Voter;

use App\Entity\Group;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;

class AdminGroupVoter extends Voter
{

    const CLASS_INDEX = 'class_index';
    const CLASS_SHOW = 'class_show';
    const CLASS_EDIT= 'class_edit';
    const CLASS_DELETE = 'class_delete';

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $group)
    {
        return in_array($attribute, [self::CLASS_INDEX,self::CLASS_SHOW,self::CLASS_EDIT,self::CLASS_DELETE])
            && $group instanceof \App\Entity\Group;
    }

    protected function voteOnAttribute($attribute, $group, TokenInterface $token)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        if (!$this->security->isGranted('ROLE_ADMIN')) {
            return false;
        }

        if ($group->getAdmin() === null) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::CLASS_INDEX:
                return $this->canChange($group, $user);
                break;
            case self::CLASS_SHOW:
                return $this->canChange($group, $user);
                break;
            case self::CLASS_EDIT:
                return $this->canChange($group, $user);
                break;
            case self::CLASS_DELETE:
                return $this->canChange($group, $user);
                break;
        }

        return false;
    }

    private function canChange(Group $group, User $user)
    {
        return $user === $group->getAdmin();
    }
    
}