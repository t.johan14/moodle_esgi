<?php

namespace App\Security\Voter;

use App\Entity\Subject;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;

class SubjectVoter extends Voter
{

    const SUBJECT_INDEX = 'subject_index';
    const SUBJECT_SHOW = 'subject_show';
    const SUBJECT_EDIT= 'subject_edit';
    const SUBJECT_DELETE = 'subject_delete';
    const SUBJECT_NEW = 'subject_new';


    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        return in_array($attribute, [self::SUBJECT_NEW,self::SUBJECT_SHOW,self::SUBJECT_EDIT,self::SUBJECT_DELETE])
            && $subject instanceof \App\Entity\Subject;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        if (!$this->security->isGranted('ROLE_ADMIN')) {
            return false;
        }

        if ($subject->getAdmin() === null) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::SUBJECT_INDEX:
                return $this->canChange($subject, $user);
                break;
            case self::SUBJECT_SHOW:
                return $this->canChange($subject, $user);
                break;
            case self::SUBJECT_EDIT:
                return $this->canChange($subject, $user);
                break;
            case self::SUBJECT_DELETE:
                return $this->canChange($subject, $user);
                break;
            case self::SUBJECT_NEW:
                return $this->canChange($subject, $user);
                break;
        }

        return false;
    }

    private function canChange(Subject $subject, User $user)
    {
        return $user === $subject->getAdmin();
    }
    
}
