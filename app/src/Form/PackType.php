<?php

namespace App\Form;

use App\Entity\Pack;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PackType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('price', NumberType::class ,[
                'label' => 'Price (ex: 9.99)'
            ])
            ->add('oldPrice', NumberType::class ,[
                'label' => 'Price before promotion otherwise currently price'
            ])
            ->add('nbStudents', IntegerType::class, [
                'label' => 'Number of students authorized for this pack',
                'attr' => ['class' => 'w-25'],
            ])
            ->add('nbrProfessors', IntegerType::class, [
                'label' => 'Number of professors authorized for this pack',
                'attr' => ['class' => 'w-25'],
            ])
            ->add('promotion', CheckboxType::class, [
                'label' => 'promotion on this pack',
                'required' => false,
            ])
            ->add('free_duration', IntegerType::class, [
                'required' => false,
                'attr' => ['class' => 'w-100'],
            ])
            ->add('type_duration', ChoiceType::class, [
                'choices'  => [
                    'hours' => 'hours',
                    'days' => 'days',
                    'weeks' => 'weeks',
                    'months' => 'months',
                    'years' => 'years',
                ],
                'required' => false,
                'attr' => ['class' => 'w-100'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Pack::class,
        ]);
    }
}
