<?php

namespace App\Controller\SuperAdmin;

use App\Entity\Transaction;
use App\Entity\User;
use App\Form\TransactionType;
use App\Repository\PackRepository;
use App\Repository\TransactionRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/transaction",name="transaction_")
 */
class TransactionController extends AbstractController
{
    /**
     * @Route("/{id}", name="index", methods={"GET"})
     */
    public function index(TransactionRepository $transactionRepository,User $user,PackRepository $packRepository): Response
    {
        $this->denyAccessUnlessGranted('access_super_admin',$this->getUser());
        
        return $this->render('super-admin/transaction/index.html.twig', [
            'transactions' =>$user->getTransactions(),
            'bill' =>$user->getTransactions()->last(),
            'pack' =>$user->getPack(),
            'user'=>$user
        ]);
    }
}
