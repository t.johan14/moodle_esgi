<?php

namespace App\Controller\Student;

use App\Entity\Exercise;
use App\Entity\Question;

use App\Entity\StudentAnswer;
use App\Entity\Subject;
use App\Entity\User;
use App\Form\EditExerciseType;
use App\Form\ExerciseType;
use App\Repository\ExerciseRepository;
use App\Repository\QuestionRepository;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/exercise",name="exercise_")
 */
class ExerciseController extends AbstractController
{
    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());

        if($this->getUser()->getStudentGroup()){
            return $this->render('student/exercise/index.html.twig', [
                'exercises' => $this->getUser()->getStudentGroup()->getExercises()
            ]);
        }else{
            return $this->render('student/exercise/index.html.twig');
        }
    }

    /**
     * @Route("/list/{subject}", name="list", methods={"GET"})
     */
    public function list(ExerciseRepository $exerciseRepository, Subject $subject): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());

        return $this->render('student/exercise/index.html.twig', [
            'exercises' => $exerciseRepository->getBySubjectAndStudent($this->getUser(), $subject)
        ]);

    }

    /**
     * @Route("/{id}", name="show", methods={"GET"})
     */
    public function show(Exercise $exercise, $error = false): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted("exerciseStudent_show", $exercise->getGroups());

        if($exercise->getSolvedBy()->contains($this->getUser())){
            $repository = $this->getDoctrine()->getRepository(Question::class);
            $answers = [];
            $lines = [];
            foreach ($exercise->getQuestions() as $question){
                $studentAnswer = $this->getDoctrine()
                    ->getRepository(StudentAnswer::class)
                    ->findOneBy(["question" => $question->getId()]);
                if($question->getType() != "code") {
                    $question->studentAnswer = $studentAnswer->getResult();
                    $question->correct = $studentAnswer->getResult() === $question->getCorrectAnswer();
                }else{
                    $studentAnswer = json_decode($studentAnswer->getResult());
                    $question->functionString = $studentAnswer->code;
                    $studentAnswer = $studentAnswer->tests;
                    $studentAnswerArray = $studentAnswer;
                    $question->studentAnswerString = json_encode($studentAnswer);
                    $question->studentAnswer = $studentAnswer;
                    $correctAnswer = $question->getCorrectAnswer();
                    $correctAnswer = json_decode($correctAnswer);
                    $correctAnswer = $correctAnswer[1];
                    $correctAnswers = [];
                    $testsInputs = [];
                    //$lines = [];
                    $i = 0;
                    foreach ($correctAnswer as $testResult){
                        $lines[] = array(
                            "test" => $testResult[0],
                            "correct" => $testResult[1],
                            "student" => $studentAnswerArray[$i]
                        );
                        $i++;
                        $correctAnswers[] = $testResult[1];
                        $testsInputs[] = $testResult[0];
                    }
                    $question->correctAnswerString = json_encode($correctAnswers);
                    $question->testsInput = json_encode($testsInputs);
                    $question->correct = $correctAnswers === $studentAnswer;
                }
                $answers[] = $question;
            }


            return $this->render('student/exercise/show_solved.html.twig', [
                'exercise' => $exercise,
                'answers' => $answers,
                'lines' => $lines
            ]);
        } else {
            foreach ($exercise->getQuestions() as $question){
                if($question->getType() == "code") {
                    $tests = json_decode($question->getCorrectAnswer(), true);
                    $question->test1 = $tests[1][0][0];
                    $question->test2 = $tests[1][1][0];
                    $question->test3 = $tests[1][2][0];
                    $question->funcName = $tests[0];
                }
            }

            $error_message = $error ? 'You must answer all the questions' : null;

            return $this->render('student/exercise/show.html.twig', [
                'exercise' => $exercise,
                'error' => $error_message,
            ]);
        }
    }

    /**
     * @Route("/submit/{id}", name="submit", methods={"POST"})
     */
    public function submit(Exercise $exercise, Request $request): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted("exerciseStudent_new", $exercise->getGroups());
        
        $answers = $request->request;
        foreach ($exercise->getQuestions() as $question){

            $studentAnswer = new StudentAnswer();
            if($question->getType() != "code"){
                if (!is_null($answers->get($question->getId())) && !empty($answers->get($question->getId()))) {
                    $studentAnswer->setResult($answers->get($question->getId()));
                } else {
                    return $this->redirectToRoute('student_exercise_show', [
                        'id' => $exercise->getId(),
                        'error' => true,
                    ]);
                }
            }else{
                $answer = [];
                $answer['code'] = $answers->get('result');
                $answer['tests'] = array( $answers->get('test1-result'), $answers->get('test2-result'), $answers->get('test3-result'));
                $answer = json_encode($answer);
                $studentAnswer->setResult($answer);
            }
            $studentAnswer->setQuestion($question);
            $studentAnswer->setStudent($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $exercise->addSolvedBy($this->getUser());
            $entityManager->persist($studentAnswer);
            $entityManager->persist($exercise);
            $entityManager->flush();
        }

        return $this->redirectToRoute('student_exercise_show', [
            'id' => $exercise->getId(),
        ]);
    }


}
