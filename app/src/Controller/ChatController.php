<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ChatController extends AbstractController
{
    /**
     * @Route("/chat/init", name="chat")
     */
    public function init(Request $request)
    {
        if (!$this->getUser()) {
            return $this->json([], 200);
        }

        if (!in_array("ROLE_PROFESSOR", $this->getUser()->getRoles()) && !in_array("ROLE_STUDENT", $this->getUser()->getRoles())) {
            return $this->json([], 200);
        }

        $token = $request->request->get('token');
        $isValidToken = $this->isCsrfTokenValid($this->getUser()->getId(), $token);
        
        if ($isValidToken) {
            $conversations = $this->getUser()->getConversations();

            if (!$this->getUser()->getStudentGroup() && !$this->getUser()->getProfessorGroups()) {
                return $this->json([
                    'conversations' => null,
                    'users_available' => [],
                ], 200);
            } else {
                $users_for_discussion = [];

                if (in_array("ROLE_PROFESSOR", $this->getUser()->getRoles())) {
                    $userGroups = $this->getUser()->getProfessorGroups();
                    if (is_null($userGroups) || empty($userGroups)) {
                        $users_for_discussion = [];
                    } else {
                        foreach($userGroups as $group) {
                            foreach($group->getGroupStudents() as $student) {
                                $elem = array(
                                    'id' => $student->getId(),
                                    'lastName' => $student->getLastName(),
                                    'firstName' => $student->getFirstName(),
                                );
                                array_push($users_for_discussion, $elem);
                            }
                        }

                    }
                } elseif (in_array("ROLE_STUDENT", $this->getUser()->getRoles())) {
                    if (is_null($this->getUser()->getStudentGroup())) {
                        $users_for_discussion = [];
                    } else {
                        $elems = $this->getUser()->getStudentGroup()->getGroupProfessors();
                        foreach($elems as $elem) {
                            $user_object = array(
                                'id' => $elem->getId(),
                                'lastName' => $elem->getLastName(),
                                'firstName' => $elem->getFirstName(),
                            );
                            array_push($users_for_discussion, $user_object);
                        }
                    }
                } else {
                    $users_for_discussion = [];
                }

                $usersToDisplay = [];
                $otherUser = '';
                $lastMessage = [];
                $lastMessageRead = [];
                $lastMessageId = [];
                $globalLastMessage = 0;
                foreach($conversations as $conversation) {
                    foreach($conversation->getParticipants() as $participant) {
                        if ($participant != $this->getUser()) {
                            $otherUser = $participant;
                        }
                    }
                    $usersToDisplay[$conversation->getId()] = $otherUser->getFirstName() . ' ' . $otherUser->getLastName();

                    if (!is_null($conversation->getLastMessageRead())) {
                        $arrayLastMessageRead = $conversation->getLastMessageRead();
                        $lastMessageRead[$conversation->getId()] = $arrayLastMessageRead[$this->getUser()->getId()];
                    } else {
                        $lastMessageRead[$conversation->getId()] = 0;
                    }
                    if ($conversation->getLastMessage() != null) {
                        $lastMessage[$conversation->getId()] = $conversation->getLastMessage()->getContent();
                        $lastMessageInConversation = $conversation->getLastMessage()->getId();
                        $lastMessageId[$conversation->getId()] = $lastMessageInConversation;
                        if ($globalLastMessage < $lastMessageInConversation) {
                            $globalLastMessage = $lastMessageInConversation;
                        }
                    } else {
                        $lastMessage[$conversation->getId()] = '';
                        $lastMessageId[$conversation->getId()] = '';
                    }
                }

                return $this->json([
                    'lastMessageRead' => $lastMessageRead,
                    'conversations' => $conversations,
                    'lastMessage' => $lastMessage,
                    'lastMessageId' => $lastMessageId,
                    'globalLastMessage' => $globalLastMessage,
                    'users_available' => $users_for_discussion,
                    'usersToDisplay' => $usersToDisplay,
                ], 200, [], ['groups' => 'conversation:read']);
            }
        } else {
            return $this->json([], 403);
        }
    }
}
