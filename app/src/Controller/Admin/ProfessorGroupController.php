<?php

namespace App\Controller\Admin;

use App\Entity\Group;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/professor-group", name="admin_professor_group_")
 */
class ProfessorGroupController extends AbstractController
{
    /**
     * @Route("/{id}", name="index", methods={"GET"})
     */
    public function index(Group $group): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted('class_edit',$group);

        $professors = $group->getGroupProfessors();
        return $this->render('admin/group_professor/index.html.twig', [
            'users' => $professors,
            'group' => $group,
        ]);
    }


    /**
     * @Route("/new/{professor}/{group}/", name="new")
     */
    public function new(User $professor, Group $group)
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted('class_edit',$group);
        $this->denyAccessUnlessGranted('class_professor_add', $professor);

        if (is_null($professor->getSubject())) {
            return $this->redirectToRoute('admin_professor_index');
            $this->addFlash("danger", "You cannot add this professor to a class");
        }
        $em = $this->getDoctrine()->getManager();
        $group->addGroupProfessor($professor);
        $em->flush();
        return $this->redirectToRoute('admin_professor_index');
    }

    /**
     * @Route("/add/{group}", name="add", methods={"POST"})
     */
    public function add(Group $group, Request $request, EntityManagerInterface $em)
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted('class_edit',$group);

        $added = 0;
        $admin = $this->getUser();
        foreach($admin->getProfessors() as $professor) {
            if (!is_null($request->get('checkbox-' . $professor->getId()))) {
                if (!is_null($professor->getSubject())) {
                    $group->addGroupProfessor($professor);
                    $added++;
                }
            }
        }

        $em->persist($group);
        $em->flush();

        if ($added > 0) {
            $this->addFlash('success', 'Students successfully added');
        } else {
            $this->addFlash('warning', 'No student added');
        }

        return $this->redirectToRoute('admin_group_index');
    }

    /**
     * @Route("/{group}", name="remove", methods={"POST"})
     */
    public function remove(Request $request, Group $group): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted('class_edit',$group);

        if ($this->isCsrfTokenValid('deleteUsers', $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();

            $professors = $this->getUser()->getProfessors();
            $count = 0;
            foreach($professors as $professor) {
                if (!is_null($request->request->get('form-checkbox-' . $professor->getId()))) {
                    $count++;
                    $group->removeGroupProfessor($professor);
                    foreach ($group->getExercises() as $exercise){
                        if($exercise->getSubject()->getProfessor() == $professor){
                            $group->removeExercise($exercise);
                        }
                    }
                    $em->persist($group);
                }
            }
        
            $em->flush();

            if ($count == 0) {
                $this->addFlash('info', 'No professor has been deleted');
            } else {
                $this->addFlash('success', 'Professor(s) deleted successfully from class');
            }
        }
        return $this->redirectToRoute('admin_group_show', ['id' => $group->getId()]);
    }
}
