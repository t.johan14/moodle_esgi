<?php

namespace App\Controller\Admin;

use App\Entity\Group;
use App\Entity\Subject;
use App\Entity\User;
use App\Form\EditProfessorType;
use App\Form\ProfessorType;
use App\Form\UploadType;
use App\Repository\GroupRepository;
use App\Repository\SubjectRepository;
use App\Repository\UserRepository;
use App\Services\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/admin-professor",name="admin_professor_")
 */
class ProfessorController extends AbstractController
{
    public MailerService $sendEmail;
    public UserPasswordEncoderInterface $encoder;

    public function __construct(MailerService $sendEmail, UserPasswordEncoderInterface $encoder)
    {
        $this->sendEmail = $sendEmail;
        $this->encoder = $encoder;
    }


    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(UserRepository $userRepository, GroupRepository $groupRepository): Response
    {
        $form = $this->createForm(UploadType::class);
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $groups = $groupRepository->findBy(["admin"=>$this->getUser()]);
        $professors = $userRepository->findByRolesAnAdminProfessor("ROLE_PROFESSOR",$this->getUser());
        $nbProfessors = count($professors);

        return $this->render('admin/professor/index.html.twig', [
            'users' => $professors,
            'groups' => $groups,
            'nbProfessors' => $nbProfessors,
            'form'=> $form->createView()
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     */
    public function new(Request $request, UserRepository $userRepository, SubjectRepository $subjectRepository, GroupRepository $groupRepository): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());

        $count_professor = $this->getUser()->getProfessors()->count();
        $subjects = $this->getUser()->getSubjects();
        $subjectsOfAdmin = [];
        foreach($subjects as $subject) {
            if (is_null($subject->getProfessor())) {
                $subjectsOfAdmin[] = $subject;
            }
        }
        $groups = $this->getUser()->getGroups();
        $user_pack_limit = $this->getUser()->getPack()->getNbrProfessors();
        if ($count_professor >= $user_pack_limit) {
            $this->addFlash('danger', 'You cannot add a new professor, you have reach the maximum limit. If you want to add new professors, you must switch to a pack with higher limits.');
            return $this->redirectToRoute('admin_professor_index');
        }

        $user = new User();
        $form = $this->createForm(ProfessorType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            if (!$userRepository->findOneBy(['email' => $user->getEmail()])) {
                $subjectIdFromForm = $request->request->get('subject');
                if (!is_null($subjectIdFromForm) && !empty($subjectIdFromForm) ) {
                    $subjectFromForm = $subjectRepository->find($subjectIdFromForm);
                } else {
                    $subjectFromForm = null;
                }
                if (!is_null($subjectFromForm)) {
                    if ($subjectFromForm->getAdmin() != $this->getUser()) {
                        $subjectFromForm = null;
                    }
                }
                $exist = false;
                $professorsOfUser = $this->getUser()->getProfessors();
                foreach($professorsOfUser as $professor) {
                    if (!is_null($professor->getSubject()) && $professor->getSubject() == $subjectFromForm) {
                        $exist = true;
                        break;
                    }
                }

                if ($exist) {
                    $this->addFlash('danger', 'The subject is already assigned to another professor');
                    return $this->render('admin/professor/new.html.twig', [
                        'user' => $user,
                        'form' => $form->createView(),
                        'subjects' => $subjectsOfAdmin,
                    ]);
                }

                $classesIds = $request->request->get('classes');
                if (!is_null($classesIds) && is_array($classesIds)) {
                    foreach($classesIds as $classId) {
                        $class = $groupRepository->find($classId);
                        if (is_null($class)) {
                            continue;
                        } elseif ($class->getAdmin() != $this->getUser()) {
                            $this->addFlash('danger', 'An error occured due to your classes selection');
                            return $this->render('admin/professor/new.html.twig', [
                                'user' => $user,
                                'form' => $form->createView(),
                                'subjects' => $subjectsOfAdmin,
                                'groups' => $groups,
                            ]);
                        } else {
                            $class->addGroupProfessor($user);
                            $entityManager->persist($class);
                        }
                    }
                }

                // Create password to professor
                $user->setPassword($this->generateRandomString(15, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'));
                $password = $user->getPassword();
                // Encode password
                $encoded = $this->encoder->encodePassword($user, $user->getPassword());
                $user->setPassword($encoded);

                //Add ROLE_PROFESSOR
                $user->setRoles(['ROLE_PROFESSOR']);

                //ADD ADMIN
                $user->setAdminProfessor($this->getUser());

                if ($subjectFromForm != null) {
                    $user->setSubject($subjectFromForm);
                    $subjectFromForm->setProfessor($user);
                    $entityManager->persist($subjectFromForm);
                }

                $entityManager->persist($user);
                $entityManager->flush();

                $this->sendEmail->sendEmail($user);

                $this->addFlash('success', 'Professor created successfully');

                return $this->redirectToRoute('admin_professor_index');
            }
            $this->addFlash('danger', 'The email is already exists!');
        }

        return $this->render('admin/professor/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'subjects' => $subjectsOfAdmin,
            'groups' => $groups,
        ]);
    }


    public function generateRandomString($length = 6, $characters = '0123456789abcdefghijklmnopqrstuvwxyz')
    {
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    /**
     * @Route("/{id}", name="show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted('professor_show', $user);

        return $this->render('admin/professor/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user, SubjectRepository $subjectRepository, GroupRepository $groupRepository, UserRepository $userRepository): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted('professor_edit', $user);

        $subjectsOfAdmin = [];
        $subjects = $this->getUser()->getSubjects();
        foreach($subjects as $subject) {
            if (is_null($subject->getProfessor())) {
                $subjectsOfAdmin[] = $subject;
            } elseif((!is_null($user->getSubject())) and ($user->getSubject() == $subject)) {
                $subjectsOfAdmin[] = $subject;
            }
        }
        $groups = $this->getUser()->getGroups();

        $form = $this->createForm(EditProfessorType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $subjectIdFromForm = $request->request->get('subject');
            if (!is_null($subjectIdFromForm) && !empty($subjectIdFromForm) ) {
                $subjectFromForm = $subjectRepository->find($subjectIdFromForm);
            } else {
                $subjectFromForm = null;
            }
            if (!is_null($subjectFromForm)) {
                if ($subjectFromForm->getAdmin() != $this->getUser()) {
                    $subjectFromForm = null;
                }
            }

            $exist = false;
            $professorsOfUser = $this->getUser()->getProfessors();
            foreach($professorsOfUser as $professor) {
                if (!is_null($professor->getSubject()) && $professor->getSubject() == $subjectFromForm && $subjectFromForm !== $user->getSubject()) {
                    $exist = true;
                    break;
                }
            }

            if ($exist) {
                $this->addFlash('danger', 'The subject is already assigned to another professor');
                return $this->render('admin/professor/new.html.twig', [
                    'user' => $user,
                    'form' => $form->createView(),
                    'subjects' => $subjectsOfAdmin,
                ]);
            }

            $classesIds = $request->request->get('classes');
            if (!is_null($classesIds) && is_array($classesIds)) {
                foreach($classesIds as $classId) {
                    $class = $groupRepository->find($classId);
                    if (is_null($class)) {
                        continue;
                    } elseif ($class->getAdmin() != $this->getUser()) {
                        $this->addFlash('danger', 'An error occured due to your classes selection');
                        return $this->render('admin/professor/new.html.twig', [
                            'user' => $user,
                            'form' => $form->createView(),
                            'subjects' => $subjectsOfAdmin,
                            'groups' => $groups,
                        ]);
                    } else {
                        $class->addGroupProfessor($user);
                        $em->persist($class);
                    }
                }
            }

            $old_subject = $user->getSubject();
            if (!is_null($old_subject)) {
                $old_subject->setProfessor(null);
                $em->persist($old_subject);
            }
            
            if (!is_null($subjectFromForm)) {
                $user->setSubject($subjectFromForm);
                $subjectFromForm->setProfessor($user);
                $em->persist($subjectFromForm);
            } else {
                $user->setSubject(null);
            }

            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Professor updated successfully');

            return $this->redirectToRoute('admin_professor_index');
        }

        return $this->render('admin/professor/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'subjects' => $subjectsOfAdmin,
            'groups' => $groups,
        ]);
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted('professor_delete',$user);
        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();

            $conversations = $user->getConversations();
            foreach($conversations as $conversation) {
                $conversation->setLastMessage(null);
                $conversation->setLastMessageRead([]);
                foreach($conversation->getMessages() as $message) {
                    $conversation->removeMessage($message);
                    $em->remove($message);
                    $em->flush();
                }
                
                $em->remove($conversation);
                $em->flush();
            }
            
            $em->remove($user);
            $em->flush();

            $this->addFlash('danger', 'Professor deleted successfully');
        }

        return $this->redirectToRoute('admin_professor_index');
    }

    /**
     * @Route("/delete", name="delete_users", methods={"POST"})
     */
    public function deleteUsers(Request $request): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        
        if ($this->isCsrfTokenValid('deleteUsers', $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();

            $professors = $this->getUser()->getProfessors();
            $count = 0;
            foreach($professors as $professor) {
                if (!is_null($request->request->get('form-checkbox-' . $professor->getId()))) {
                    $conversations = $professor->getConversations();
                    foreach($conversations as $conversation) {
                        $conversation->setLastMessage(null);
                        $conversation->setLastMessageRead([]);
                        foreach($conversation->getMessages() as $message) {
                            $conversation->removeMessage($message);
                            $em->remove($message);
                            $em->flush();
                        }
                        $em->remove($conversation);
                    }
                    $count++;
                    $em->remove($professor);
                }
            }
        
            $em->flush();

            if ($count == 0) {
                $this->addFlash('info', 'No professor has been deleted');
            } else {
                $this->addFlash('success', 'Professor(s) deleted successfully');
            }
        }

        return $this->redirectToRoute('admin_professor_index');
    }


    /**
     * @Route("/{group}/{professor}", name="remove", methods={"DELETE"})
     */
    public function remove(Request $request, Group $group, User $professor): Response
    {
        $this->denyAccessUnlessGranted('access_menu', $this->getUser());
        $this->denyAccessUnlessGranted('class_edit', $group);
        $this->denyAccessUnlessGranted('professor_edit', $professor);

        if ($this->isCsrfTokenValid('remove' . $professor->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $$group->removeGroupProfessor($professor);
            $entityManager->flush();

            $this->addFlash('danger', 'Professor removed successfully');
        }
        return $this->redirectToRoute('admin_professor_group_index', ['id' => $group->getId()]);
    }



    /**
     * @Route("/import", name="import",methods={"POST"})
     */
    public function importCSV(Request $request,SubjectRepository $subjectRepository,UserRepository $userRepository,EntityManagerInterface $em): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        
        $admin = $this->getUser();
        $form = $this->createForm(UploadType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $count_professor = $admin->getProfessors()->count();
            $user_pack_limit = $admin->getPack()->getNbrProfessors();
            if ($count_professor >= $user_pack_limit) {
                $this->addFlash('danger', 'You cannot add a new professor, you have reach the maximum limit. If you want to add new professors, you must switch to a pack with higher limits.');
                return $this->redirectToRoute('admin_professor_index');
            }

            $file = $form['upload_file']->getData();
            if ($file)
            {
                $rowNo = 1;
                if (($fp = fopen($file, "r")) !== FALSE) {
                    while (($row = fgetcsv($fp, 1000, ";")) !== FALSE) {
                        $num = count($row);
                        if ($num === 4 || $num === 3){
                            if($row[0] !=="firstName" && $row[1] !=="lastName" && $row[2] !=="email"){
                                $user = new User();

                                $rowNo++;
                                $user->setFirstName($row[0]);
                                $user->setLastName($row[1]);
                                $user->setEmail($row[2]);
                                if ($num === 4){
                                    if(is_null($subjectRepository->findOneBy(["name"=>$row[3]]))){
                                        $subject = new Subject();
                                        $subject->setName($row[3]);
                                        $subject->setAdmin($admin);
                                        $em->persist($subject);
                                        $em->flush();
                                    }
                                    $user->setSubject($subjectRepository->findOneBy(["name"=>$row[3]]));
                                }

                                $entityManager = $this->getDoctrine()->getManager();
                                if (!$userRepository->findOneBy(['email' => $user->getEmail()])) {

                                    // Create password to student
                                    $user->setPassword($this->generateRandomString(15, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'));
                                    $password = $user->getPassword();
                                    // Encode password
                                    $encoded = $this->encoder->encodePassword($user, $user->getPassword());
                                    $user->setPassword($encoded);

                                    //Add ROLE_STUDENT
                                    $user->setRoles(['ROLE_PROFESSOR']);

                                    //ADD ADMIN
                                    $user->setAdminProfessor($this->getUser());

                                    $entityManager->persist($user);
                                    $entityManager->flush();
                                    $this->sendEmail->sendEmail($user);
                                    $this->addFlash('success', 'Professor created successfully');
                                }else{
                                    $this->addFlash('danger', 'The email is already exists!');
                                }
                            }
                        }
                    }
                    fclose($fp);
                }
            }
        }
        return $this->redirectToRoute('admin_professor_index');
    }


}
