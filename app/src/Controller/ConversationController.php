<?php

namespace App\Controller;

use App\Entity\Conversation;
use App\Repository\ConversationRepository;
use App\Repository\MessageRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/conversations", name="conversation")
 */
class ConversationController extends AbstractController
{
    /**
     * @Route("/new", name="_new", methods={"POST"})
     */
    public function new(Request $request, UserRepository $userRepository, EntityManagerInterface $em)
    {
        if(!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('status' => 'Error'),405);
        }
        if(!isset($request->request)) {
            return new JsonResponse(array('status' => 'Error'),403);
        }

        $token = $request->request->get('token');
        $isValidToken = $this->isCsrfTokenValid($this->getUser()->getId(), $token);
        
        if ($isValidToken) {
            $otherUser = $request->request->get('otherUser');
            if (!is_null($otherUser)) {
                $otherUser = $userRepository->find($otherUser);
            }

            if ($otherUser->getId() === $this->getUser()->getId()) {
                return $this->json([
                    'message' => 'You cannot create a conversation with yourself'
                ], 403);
            }
    
            $conversations = $this->getUser()->getConversations();
            
            if (!is_null($conversations) && !empty($conversations)) {
                foreach($conversations as $conversation) {
                    if ($conversation->getParticipants()->contains($otherUser)) {
                        return $this->json([
                            'message' => 'A conversation already exist with ' . $otherUser->getFirstName() . ' ' . $otherUser->getLastName()
                        ], 500);
                    }
                }
            }

            $this->denyAccessUnlessGranted("conversation_add", $otherUser);

            $conversation = new Conversation();
            $conversation->addParticipant($this->getUser());
            $conversation->addParticipant($otherUser);
            $lastMessageRead[$otherUser->getId()] = 0;
            $lastMessageRead[$this->getUser()->getId()] = 0;
            $conversation->setLastMessageRead($lastMessageRead);

            $em->persist($conversation);
            $em->flush();

            $userToDisplay = $otherUser->getFirstName() . ' ' . $otherUser->getLastName();

            return $this->json([
                'conversationId' => $conversation->getId(),
                'userToDisplay' => $userToDisplay,
            ], 201);

        }
        return $this->json([
            ''
        ], 403);
    }


    /**
     * @Route("/show", name="_show", methods={"POST"})
     */
    public function show(Request $request, ConversationRepository $conversationRepository, MessageRepository $messageRepository, EntityManagerInterface $em)
    {
        if(!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('status' => 'Error'),405);
        }
        if(!isset($request->request)) {
            return new JsonResponse(array('status' => 'Error'),403);
        }

        $token = $request->request->get('token');
        $isValidToken = $this->isCsrfTokenValid($this->getUser()->getId(), $token);
        
        if ($isValidToken) {
            $conversationId = $request->get('conversation');
            $conversation = $conversationRepository->find($conversationId);
            $this->denyAccessUnlessGranted('conversation_view', $conversation);

            $messages = $messageRepository->findMessageByConversationId($conversation->getId());
            $otherUser = null;

            foreach($conversation->getParticipants() as $participant) {
                if ($participant != $this->getUser()) {
                    $otherUser = $participant;
                }
            }

            if (is_null($otherUser)) {
                return $this->json([''], 500);
            }

            $authors = [];
            $dates = [];
            foreach($messages as $message) {
                $authors[$message->getId()] = $message->getAuthor()->getId();
                $dates[$message->getId()] = $message->getCreatedAt()->format('d/m/Y H:i');
            }

            if ($conversation->getLastMessage() != null) {
                $lastMessageRead = $conversation->getLastMessageRead();
                $lastMessageRead[$this->getUser()->getId()] = $conversation->getLastMessage()->getId();
                $conversation->setLastMessageRead($lastMessageRead);

                $em->persist($conversation);
                $em->flush();
            }

            return $this->json([
                'authors' => $authors,
                'datesMessages' => $dates,
                'messages' => $messages,
                'conversationId' => $conversation->getId(),
                'lastMessage' => $conversation->getLastMessage(),
                'participant_firstName' => $otherUser->getFirstName(),
                'participant_lastName' => $otherUser->getLastName(),
                'myId' => $this->getUser()->getId(),
            ], 200, [], ['groups' => 'message:read']);
        }

        return $this->json([''], 403);
    }


    /**
     * @Route("/refresh", name="_refresh", methods={"GET"})
     */
    public function refresh(Request $request, ConversationRepository $conversationRepository, MessageRepository $messageRepository, EntityManagerInterface $em)
    {
        if(!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('status' => 'Error'),405);
        }
        if(!isset($request->query)) {
            return new JsonResponse(array('status' => 'Error'),403);
        }

        $conversationId = $request->query->get('conversation');
        $lastMessage = $request->query->get('lastMessage');

        if (empty($lastMessage) || empty($conversationId)) {
            return $this->json(['empty' => true], 200);
        }

        $conversation = $conversationRepository->find($conversationId);

        $this->denyAccessUnlessGranted('conversation_view', $conversation);

        $messages = $messageRepository->findMessageRefreshedByConversationId($conversation->getId(), $lastMessage);
        $otherUser = null;

        foreach($conversation->getParticipants() as $participant) {
            if ($participant != $this->getUser()) {
                $otherUser = $participant;
            }
        }

        if (is_null($otherUser)) {
            return $this->json([''], 500);
        }

        $authors = [];
        $dates = [];
        foreach($messages as $message) {
            $authors[$message->getId()] = $message->getAuthor()->getId();
            $dates[$message->getId()] = $message->getCreatedAt()->format('d/m/Y H:i');
        }

        if ($conversation->getLastMessage() != null) {
            $lastMessageRead = $conversation->getLastMessageRead();
            $lastMessageRead[$this->getUser()->getId()] = $conversation->getLastMessage()->getId();
            $conversation->setLastMessageRead($lastMessageRead);

            $em->persist($conversation);
            $em->flush();
        }

        return $this->json([
            'empty' => false,
            'authors' => $authors,
            'datesMessages' => $dates,
            'messages' => $messages,
            'conversationId' => $conversation->getId(),
            'participant_firstName' => $otherUser->getFirstName(),
            'participant_lastName' => $otherUser->getLastName(),
            'myId' => $this->getUser()->getId(),
        ], 200, [], ['groups' => 'message:read']);
    }


    /**
     * @Route("/refresh_all", name="_refresh_all", methods={"GET"})
     */
    public function refreshAll(Request $request, ConversationRepository $conversationRepository, MessageRepository $messageRepository)
    {
        if(!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('status' => 'Error'),405);
        }
        if(!isset($request->query)) {
            return new JsonResponse(array('status' => 'Error'),403);
        }

        if (!in_array("ROLE_PROFESSOR", $this->getUser()->getRoles()) && !in_array("ROLE_STUDENT", $this->getUser()->getRoles())) {
            return $this->json([], 200);
        }

        $token = $request->query->get('token');
        $isValidToken = $this->isCsrfTokenValid($this->getUser()->getId(), $token);
        
        if ($isValidToken) {

            $conversations = $this->getUser()->getConversations();

            $usersToDisplay = [];
            $otherUser = '';
            $lastMessage = [];
            $lastMessageRead = [];
            $lastMessageId = [];
            $globalLastMessage = 0;

            foreach($conversations as $conversation) {
                foreach($conversation->getParticipants() as $participant) {
                    if ($participant != $this->getUser()) {
                        $otherUser = $participant;
                    }
                }
                $usersToDisplay[$conversation->getId()] = $otherUser->getFirstName() . ' ' . $otherUser->getLastName();

                if (!is_null($conversation->getLastMessageRead())) {
                    $arrayLastMessageRead = $conversation->getLastMessageRead();
                    $lastMessageRead[$conversation->getId()] = $arrayLastMessageRead[$this->getUser()->getId()];
                } else {
                    $lastMessageRead[$conversation->getId()] = 0;
                }
                if ($conversation->getLastMessage() != null) {
                    $lastMessage[$conversation->getId()] = $conversation->getLastMessage()->getContent();
                    $lastMessageInConversation = $conversation->getLastMessage()->getId();
                    $lastMessageId[$conversation->getId()] = $lastMessageInConversation;
                    if ($globalLastMessage < $lastMessageInConversation) {
                        $globalLastMessage = $lastMessageInConversation;
                    }
                } else {
                    $lastMessage[$conversation->getId()] = '';
                    $lastMessageId[$conversation->getId()] = '';
                }
            }

            return $this->json([
                'empty' => false,
                'lastMessage' => $lastMessage,
                'lastMessageRead' => $lastMessageRead,
                'lastMessageId' => $lastMessageId,
                'globalLastMessage' => $globalLastMessage,
                'conversations' => $conversations,
                'usersToDisplay' => $usersToDisplay,
            ], 200, [], ['groups' => 'conversation:read']);
        } else {
            return $this->json(['empty' => true], 403);
        }
    }
}
