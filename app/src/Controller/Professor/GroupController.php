<?php

namespace App\Controller\Professor;

use App\Entity\Group;
use App\Entity\User;
use App\Form\GroupType;
use App\Repository\GroupRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/group", name="group_")
 */
class GroupController extends AbstractController
{
    /**
     * @Route("/{id}", name="index", methods={"GET"})
     */
    public function index(User $professor): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted("group_show", $professor);
        
        return $this->render('professor/group/index.html.twig', [
            'groups' => $professor->getProfessorGroups(),
        ]);
    }

    /**
     * @Route("/add-exercises/{id}", name="addExercises", methods={"POST"})
     */
    public function addExercises(Group $group, Request $request, EntityManagerInterface $em): Response
    {
        $this->denyAccessUnlessGranted('access_menu',$this->getUser());
        $this->denyAccessUnlessGranted("group_edit", $group);

        $added = 0;
        $professor = $this->getUser();
        foreach($professor->getSubject()->getExercises() as $exercise) {
            if (!is_null($request->get('checkbox-' . $exercise->getId()))) {
                $exercise->addGroup($group);
                $added++;
            }
        }

        $em->persist($group);
        $em->flush();

        if ($added > 0) {
            $this->addFlash('success', 'Exercise(s) successfully added');
            $successMessage = 'Exercise(s) successfully added';
        } else {
            $this->addFlash('warning', 'No exercise added');
            $WarningMessage = 'No exercise added';
        }
        
        return $this->redirectToRoute('professor_group_index', [
            'id' => $this->getUser()->getId(),
        ]);
    }
}
